/* ----------------------------------------------------------------------
"Pixel dust" Protomatter library example. As written, this is
SPECIFICALLY FOR THE ADAFRUIT MATRIXPORTAL M4 with 64x32 pixel matrix.
Change "HEIGHT" below for 64x64 matrix. Could also be adapted to other
Protomatter-capable boards with an attached LIS3DH accelerometer.

PLEASE SEE THE "simple" EXAMPLE FOR AN INTRODUCTORY SKETCH,
or "doublebuffer" for animation basics.
------------------------------------------------------------------------- */

#include <Wire.h>                 // For I2C communication
#include <Adafruit_LIS3DH.h>      // For accelerometer
#include <Adafruit_PixelDust.h>   // For sand simulation
#include <Adafruit_Protomatter.h> // For RGB matrix
#include "bitmaps.h"

#define HEIGHT 32  // Matrix height (pixels) - SET TO 64 FOR 64x64 MATRIX!
#define WIDTH 64   // Matrix width (pixels)
#define MAX_FPS 45 // Maximum redraw rate, frames/second
#define MAX_FPS_PONG 25
#define MAX_FPS_SHIP 15

#define CLICKTHRESHHOLD 120

#if HEIGHT == 64 // 64-pixel tall matrices have 5 address lines:
uint8_t addrPins[] = {17, 18, 19, 20, 21};
#else // 32-pixel tall matrices have 4 address lines:
uint8_t addrPins[] = {17, 18, 19, 20};
#endif

/* Setup board */

// Remaining pins are the same for all matrix sizes. These values
// are for MatrixPortal M4. See "simple" example for other boards.
uint8_t rgbPins[] = {7, 8, 9, 10, 11, 12};
uint8_t clockPin = 14;
uint8_t latchPin = 15;
uint8_t oePin = 16;
uint8_t UP_PIN = 2;
uint8_t DOWN_PIN = 3;

Adafruit_Protomatter matrix(
    WIDTH, 4, 1, rgbPins, sizeof(addrPins), addrPins,
    clockPin, latchPin, oePin, true);

Adafruit_LIS3DH accel = Adafruit_LIS3DH();

/* Setup app */

#define N_COLORS 8
#define BOX_HEIGHT 8
#define N_GRAINS 800

uint16_t colors[N_COLORS];
uint16_t color;

Adafruit_PixelDust rainbowSand(WIDTH, HEIGHT, N_GRAINS, 1, 128, false);

uint32_t prevTime = 0; // Used for frames-per-second throttle
int scene = 0;

// SETUP - RUNS ONCE AT PROGRAM START --------------------------------------
void err(int x)
{
  uint8_t i;
  pinMode(LED_BUILTIN, OUTPUT); // Using onboard LED
  for (i = 1;; i++)
  {                                   // Loop forever...
    digitalWrite(LED_BUILTIN, i & 1); // LED on/off blink to alert user
    delay(x);
  }
}

void setup(void)
{
  Serial.begin(115200);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  //while (!Serial) delay(10);

  randomSeed(analogRead(A0));

  ProtomatterStatus status = matrix.begin();
  Serial.printf("Protomatter begin() status: %d\n", status);

  if (!rainbowSand.begin())
  {
    Serial.println("Couldn't start sand");
    err(1000); // Slow blink = malloc error
  }

  if (!accel.begin(0x19))
  {
    Serial.println("Couldn't find accelerometer");
    err(250); // Fast bink = I2C error
  }
  accel.setRange(LIS3DH_RANGE_4_G); // 2, 4, 8 or 16 G!
  accel.setClick(2, CLICKTHRESHHOLD);

  //rainbowSand.randomize(); // Initialize random sand positions
  //setSandToColorBlocks(rainbowSand);

  color = matrix.color565(255, 0, 40);

  colors[0] = matrix.color565(64, 64, 64);  // Dark Gray
  colors[1] = matrix.color565(120, 79, 23); // Brown
  colors[2] = matrix.color565(228, 3, 3);   // Red
  colors[3] = matrix.color565(255, 140, 0); // Orange
  colors[4] = matrix.color565(255, 237, 0); // Yellow
  colors[5] = matrix.color565(0, 128, 38);  // Green
  colors[6] = matrix.color565(0, 77, 255);  // Blue
  colors[7] = matrix.color565(117, 7, 135); // Purple

  //listen for up/down button press
  attachInterrupt(digitalPinToInterrupt(UP_PIN), up, FALLING);
  attachInterrupt(digitalPinToInterrupt(DOWN_PIN), down, FALLING);


  //snake init
  resetSnake();
}

void up()
{
  if (scene == 0)
  {
    scene++;
  } else if (scene == 2) {
    scene = 50; //wrong
  }
}
void down()
{
  if (scene == 0) {
    scene = 50;//wrong
  } else if (scene == 2) {
    scene++;
  }
}

// MAIN LOOP - RUNS ONCE PER FRAME OF ANIMATION ----------------------------

void loop()
{
  uint8_t click = accel.getClick();
//  if (click == 0) return;
//  if (! (click & 0x30)) return;
//  Serial.print("Click detected (0x"); Serial.print(click, HEX); Serial.print("): ");
//  if (click & 0x10) Serial.print(" single click");
  if (click & 0x20 && scene == 5) {
    Serial.print(" double click");
    scene++;
  }
  
  switch (scene)
  {
  case 0:
    game();
    break;

  case 1:
    firstYes();
    break;

  case 2:
    firstDown();
    break;

  case 3:
    firstChallenge();
    break;

  case 4:
    asteroids();
    break;

  case 5:
    rachel();
    break;

  case 6:
    makeSnake();
    break;
    
  case 7:
    merryChristmas();
    break;

  case 50:
    wrong();
    break;

  default:
    drawEnd();
  }
  matrix.show();
}

int gameMillis=0;
void game()
{
  if (gameMillis == 0) {
    gameMillis = millis();
  }
  int currentMillis = millis() - gameMillis;
  resetScreen();

//  int animationMillis = (currentMillis % 5000);
//  float progress = float(animationMillis) / 5000;
//  int colorChannel = progress * 255;

  matrix.setTextColor(colors[1]);
  
  matrix.print(" Hi Bernie");
  if (currentMillis > 1000)
  {
    matrix.setCursor(10, 8);
    matrix.print("Want to");
    matrix.setCursor(17, 16);
    matrix.print("play");
    matrix.setCursor(10, 24);
    matrix.print("a game?");
  }
  if (currentMillis > 6000) {
    resetScreen();
    matrix.setCursor(3,4);
    matrix.print("What goes");
    matrix.setCursor(3,12);
    matrix.print("down, must");
    matrix.setCursor(3,20);
    matrix.print("come __");
  }
}

void firstDown()
{
  resetScreen();
  matrix.setTextColor(matrix.color565(30,129,45));
    matrix.setCursor(14, 4);
    matrix.print("System");
    matrix.setCursor(14, 12);
    matrix.print("of a");
    matrix.setCursor(14, 20);
    matrix.print("____");
}

int yesStart=0;
void firstYes()
{
  if (yesStart == 0) {
    yesStart = millis();
  }
  int currentMillis = millis() - yesStart;
  resetScreen();
  if (currentMillis < 3000)
  {
    matrix.drawBitmap(0, 0, epd_bitmap_6536440_preview, 64, 32, matrix.color565(30, 30, 30));
  }
  else if (currentMillis < 6000)
  {
    matrix.setCursor(16, HEIGHT / 2.5);
    matrix.print("Great!");
  } else if (currentMillis < 9000) {
    matrix.setCursor(0,0);
    matrix.print("Try this");
  } else {
    scene++;
  }
}

int circleX = WIDTH / 2;
int circleR = 3;
int hits[5];
int password[] = {1, 2, 2, 1, 2};
int hitIndex = 0;
void firstChallenge()
{
  //slide from one side to the other
  resetScreen();

  // Limit the animation frame rate to MAX_FPS.  Because the subsequent sand
  // calculations are non-deterministic (don't always take the same amount
  // of time, depending on their current states), this helps ensure that
  // things like gravity appear constant in the simulation.
  uint32_t t;
  while (((t = micros()) - prevTime) < (1000000L / MAX_FPS_PONG))
    ;
  prevTime = t;

  // Read accelerometer...
  sensors_event_t event;
  accel.getEvent(&event);
  //Serial.printf("(%0.1f, %0.1f, %0.1f)\n", event.acceleration.x, event.acceleration.y, event.acceleration.z);

  double xx, yy, zz;
  xx = event.acceleration.x;
  yy = event.acceleration.y;
  zz = event.acceleration.z;

  if (xx > 0)
  {
    circleX++;
  }
  else
  {
    circleX--;
  }
  //circleX += int(xx / 100);
  boolean touchingLeft = false;
  boolean touchingRight = false;
  int leftBound = circleR + 1;
  int rightBound = WIDTH - circleR - 2;

  if (circleX < leftBound)
  {
    circleX = leftBound;
  }
  if (circleX == circleR + 1)
  {
    touchingLeft = true;
  }
  if (circleX > rightBound)
  {
    circleX = rightBound;
  }
  if (circleX == rightBound)
  {
    touchingRight = true;
  }

  if (touchingLeft || touchingRight)
  {
    hits[hitIndex] = touchingLeft ? 1 : 2;
    if (hitIndex < 4)
    {
      hitIndex++;
    }
    else
    {
      boolean correct = true;
      for (int i = 0; i < 5; i++)
      {
        if (hits[i] != password[i])
        {
          correct = false;
        }
      }

      if (correct)
      {
        showCorrect();
        delay(5000);
        scene++;
      }
      else
      {
        memset(hits, 0, sizeof(hits));
        hitIndex = 0;
      }
    }
  }

  for (int i = 0; i < 5; i++)
  {
    int colorIndex = hits[i] > 0 ? hits[i] == password[i] ? 3 : 2 : 5;
    matrix.drawPixel(i, 0, colors[colorIndex]);
  }

  matrix.drawLine(0, 8, 0, HEIGHT - 8, touchingLeft ? colors[4] : colors[2]);
  matrix.drawLine(WIDTH - 1, 8, WIDTH - 1, HEIGHT - 8, touchingRight ? colors[6] : colors[2]);

  if (touchingLeft || touchingRight)
  {
    matrix.drawCircle(circleX, HEIGHT / 2, circleR - 1, colors[4]);
    matrix.show();
    delay(100);
    matrix.drawCircle(circleX, HEIGHT / 2, circleR - 2, colors[4]);
    matrix.show();
    delay(100);
    matrix.drawCircle(circleX, HEIGHT / 2, circleR - 3, colors[4]);
    matrix.show();

    delay(1000);
    circleX = WIDTH / 2;
    matrix.drawCircle(circleX, HEIGHT / 2, circleR, colors[4]);
    matrix.show();
  }
  else
  {
    matrix.drawCircle(circleX, HEIGHT / 2, circleR, colors[3]);
  }
}

void showCorrect()
{
  resetScreen();
  matrix.setCursor(4, 4);
  matrix.print("you");
  matrix.setCursor(4, 12);
  matrix.print("cracked");
  matrix.setCursor(4,20);
  matrix.print("the code!");
  matrix.show();
}

int shipX = WIDTH - 10;
int shipY = HEIGHT / 2 - 4;
int asteroidStartTime = 0;
#define rocks 32
int rockXStart[rocks] = {-5, -15, -25, -35, -40, -65, -75, -80, -85, -95, -100, -102, -116, -120, -125, -129, -133, -135, -140, -145, -155, -155, -157, -158, -163, -165, -170, -175, -175, -175, -177, -178};
int rockX[rocks];
int rockY[rocks] = {4, 12, 18, 26, 4, 20, 22, 30, 4, 12, 18, 26, 4, 20, 13, 20, 31, 4, 12, 18, 22, 8, 20, 26, 4, 14, 6, 18, 10, 16, 22, 30};

void asteroids()
{
  // Limit the animation frame rate to MAX_FPS.  Because the subsequent sand
  // calculations are non-deterministic (don't always take the same amount
  // of time, depending on their current states), this helps ensure that
  // things like gravity appear constant in the simulation.
  uint32_t t;
  while (((t = micros()) - prevTime) < (1000000L / MAX_FPS_SHIP))
    ;
  prevTime = t;

  if (asteroidStartTime == 0)
  {
    asteroidStartTime = millis();
  }
  int currentTime = millis() - asteroidStartTime;

  // Read accelerometer...
  sensors_event_t event;
  accel.getEvent(&event);
  //Serial.printf("(%0.1f, %0.1f, %0.1f)\n", event.acceleration.x, event.acceleration.y, event.acceleration.z);

  double yy;
  yy = event.acceleration.y;

  //move ship
  shipY += yy;
  if (shipY > HEIGHT - 6)
  {
    shipY = HEIGHT - 6;
  }
  if (shipY < -1)
  {
    shipY = -1;
  }

  //move rock
  resetScreen();
  int sy = shipY + 3;
  bool collision = false;

  //move and draw rocks, detect collision with ship
  for (int i = 0; i < rocks; i++)
  {

    //calc rock x pos

    int xOffset = int(currentTime / 100);
    Serial.println(xOffset);
    rockX[i] = xOffset + rockXStart[i];

    //draw rock
    matrix.drawCircle(rockX[i], rockY[i], 1, colors[0]);

    int xDiff = abs(shipX + 1 - rockX[i]);
    bool xHit = xDiff > 0 && xDiff < 3;

    int yDiff = abs((sy + 1) - rockY[i]);
    bool yHit = yDiff > 0 && yDiff < 3;
    
    if (xHit && yHit)
    {
      collision = true;
    }
  }


  //ship
  matrix.drawChar(shipX, shipY, 0xAE, colors[1], colors[1], 1);
  matrix.drawPixel(shipX, sy, colors[5]);

  if (collision == true)
  {
    matrix.setCursor(22, 12);
    matrix.print("HIT!");
    matrix.show();
    delay(1000);
    resetScreen();
    collision = false;
    asteroidStartTime = millis();
  }
  
  int lastRock = rocks - 1;
  if (rockX[lastRock] > WIDTH) {
    //row of rocks that looks impossible but then you win
    resetScreen();
    matrix.setCursor(4, 8);
    matrix.print("Nice");
    matrix.setCursor(4,16);
    matrix.print("Flying!");
    matrix.show();
    delay(3000);
    scene++;
  }
}

void merryChristmas()
{
  resetScreen();
  matrix.setTextColor(matrix.color565(50, 0, 70));
  // matrix.print("   Merry Xmas Dad!Love Katie  & Dylan");
  matrix.setCursor(0, 0);
  matrix.print("Merry Xmas");
  matrix.setCursor(18, 8);
  matrix.print("Dad!");
  matrix.setCursor(0, 16);
  matrix.print("Love Katie");
  matrix.setCursor(5, 24);
  matrix.print("& Dylan");
  matrix.show();
  delay(5000);
  scene++;
}

void resetScreen()
{
  matrix.fillScreen(matrix.color565(0, 0, 0));
  matrix.setCursor(0, 0);
}

void drawSand(bool colorful)
{
  // Limit the animation frame rate to MAX_FPS.  Because the subsequent sand
  // calculations are non-deterministic (don't always take the same amount
  // of time, depending on their current states), this helps ensure that
  // things like gravity appear constant in the simulation.
  uint32_t t;
  while (((t = micros()) - prevTime) < (1000000L / MAX_FPS));
  prevTime = t;

  // Read accelerometer...
  sensors_event_t event;
  accel.getEvent(&event);
  //Serial.printf("(%0.1f, %0.1f, %0.1f)\n", event.acceleration.x, event.acceleration.y, event.acceleration.z);

  double xx, yy, zz;
  xx = event.acceleration.x * 1000;
  yy = event.acceleration.y * 1000;
  zz = event.acceleration.z * 1000;

  // Run one frame of the simulation
  rainbowSand.iterate(xx, yy, zz);

  //sand.iterate(-accel.y, accel.x, accel.z);

  // Update pixel data in LED driver
  dimension_t x, y;
  matrix.fillScreen(0x0);
  for (int i = 0; i < N_GRAINS; i++)
  {
    rainbowSand.getPosition(i, &x, &y);
    int n = (i / ((WIDTH / N_COLORS) * BOX_HEIGHT))%N_COLORS; // Color index
    uint16_t flakeColor = colorful ? colors[n] : colors[1];

    matrix.drawPixel(x, y, flakeColor);
    //Serial.printf("(%d, %d)\n", x, y);
  }
}

void setSandToColorBlocks(Adafruit_PixelDust sand)
{
  //Set up initial sand coordinates, in 8x8 blocks
  int n = 0;
  for (int i = 0; i < N_COLORS; i++)
  {
    int xx = i * WIDTH / N_COLORS;
    int yy = HEIGHT - BOX_HEIGHT;
    for (int y = 0; y < BOX_HEIGHT; y++)
    {
      for (int x = 0; x < WIDTH / N_COLORS; x++)
      {
        Serial.printf("#%d -> (%d, %d)\n", n,  xx + x, yy + y);
        sand.setPosition(n++, xx + x, yy + y);
      }
    }
  }
  Serial.printf("%d total pixels\n", n);
}

void wrong() {
  resetScreen();
  matrix.setTextColor(matrix.color565(255,0,0));
  matrix.setCursor(0,0);
  matrix.print("Wrong!");
  matrix.setCursor(0,8);
  matrix.print("you must");
  matrix.setCursor(0,16);
  matrix.print("reset!");
}

void rachel() {
  drawSand(false);
  matrix.setCursor(1, 1);
  matrix.setTextColor(matrix.color565(0, 0, 0));
  matrix.print("Ask");
  matrix.setCursor(1,9);
  matrix.print("Rachel");
}

#define SNAKE_FPS 7
#define SNAKE_INITIAL 20
#define SNAKE_SIZE 100
int snakeStart=0;
int snake[SNAKE_SIZE][2];
int snakeDirection=0;
int snakeLength;
int foodPos[2];
int fps;

void makeSnake() {
    // Limit the animation frame rate to MAX_FPS.  Because the subsequent sand
  // calculations are non-deterministic (don't always take the same amount
  // of time, depending on their current states), this helps ensure that
  // things like gravity appear constant in the simulation.
  uint32_t t;
  while (((t = micros()) - prevTime) < (1000000L / fps))
    ;
  prevTime = t;

  if (snakeStart == 0) {
    snakeStart = millis();
  }
  int currentTime = millis() - snakeStart;
  
  resetScreen();

  if (currentTime < 4000) {
    //write an intro message
    matrix.setTextColor(colors[1]);
    matrix.setCursor(4,4);
    matrix.print("the snake");
    matrix.setCursor(4,12);
    matrix.print("is hungry");
    matrix.show();
  } else {
    //snake game
    sensors_event_t event;
    accel.getEvent(&event);
    //Serial.printf("(%0.1f, %0.1f, %0.1f)\n", event.acceleration.x, event.acceleration.y, event.acceleration.z);
  
    double xx, yy;
    xx = event.orientation.x;
    yy = event.orientation.y;
  
  
    //set accell direction
    float diff = abs(yy - xx);

    if (diff > 1) {
      if (abs(yy) > abs(xx) && snakeDirection >= 3) {
        snakeDirection = yy > 0 ? 2 : 1;
      } else if (abs(xx) > abs(yy) && snakeDirection <= 2) {
        snakeDirection = xx > 0 ? 4 : snakeDirection > 0 ? 3 : 1;
      }
    }
    if (snakeDirection == 0) {
      snakeDirection = 1;
    }

    Serial.println(snakeDirection);
  
    matrix.drawPixel(foodPos[0],foodPos[1],colors[4]);
    moveSnake();
    
    for (int i=0; i<SNAKE_SIZE; i++) {
      if (snake[i][0] > -1) {
        matrix.drawPixel(snake[i][0],snake[i][1],colors[1]);
      }
    }
  
    //collision detection
    if (snake[0][0] == foodPos[0] && snake[0][1] == foodPos[1]) {
      growSnake();
      makeFood();
    }
    
    for (int i=snakeLength; i>0; i--) {
      if (snake[0][0] == snake[i][0] && snake[0][1] == snake[i][1]) {
        matrix.drawPixel(snake[0][0],snake[0][1],colors[2]);
        snakeLose();
      }
    } 
  }
}
void moveSnake() {
  for (int i=snakeLength-1; i>0; i--) {
    snake[i][0] = snake[i-1][0];
    snake[i][1] = snake[i-1][1];
  }
  switch (snakeDirection) {
    case 1://up
      snake[0][1]--;
      break;
    case 2://down
      snake[0][1]++;
      break;
    case 3://left
      snake[0][0]--;
      break;
    case 4://right
      snake[0][0]++;
      break;
  }
  if (snake[0][0] >= WIDTH) {
    snake[0][0] -= WIDTH;
  }
  if (snake[0][0] < 0) {
    snake[0][0] += WIDTH;
  }
  if (snake[0][1] >= HEIGHT) {
    snake[0][1] -= HEIGHT;
  }
  if (snake[0][1] < 0) {
    snake[0][1] += HEIGHT;
  }
}
void resetSnake() {
  snakeLength = SNAKE_INITIAL;
  fps = SNAKE_FPS;
  snake[0][0] = WIDTH/2;
  snake[0][1] = HEIGHT/2;
  snakeDirection = 0;
  for (int i=1; i<SNAKE_SIZE; i++) {
    if (i < SNAKE_INITIAL) {
      snake[i][0] = WIDTH/2 - i;
      snake[i][1] = HEIGHT/2;
    } else {
      snake[i][0] = -1;
      snake[i][1] = -1;
    }
  }
  makeFood();
}
void growSnake() {
  if (snakeLength < SNAKE_SIZE) {
    snakeLength += 8;
    fps += 1;
  } else {
    snakeWin();
  }
}
void snakeWin() {
  resetScreen();
  matrix.setCursor(0,0);
  matrix.setTextColor(colors[1]);
  matrix.print("You Win!");
  matrix.show();
  delay(2000);
  scene++;
}
void snakeLose() {
  matrix.show();
  delay(3000);
  resetScreen();
  matrix.setCursor(0,0);
  matrix.print("You Lose!");
  matrix.show();
  resetSnake();
  delay(3000);
}

void randomCoords(int pos[]) {
  pos[0] = random(0,WIDTH);
  pos[1] = random(0,HEIGHT);
}
void makeFood() {
  randomCoords(foodPos);
  for (int i=0; i<SNAKE_SIZE; i++) {
    if (snake[i][0] == foodPos[0] && snake[i][1] == foodPos[1]) {
     makeFood();
    }
  }
}

void drawEnd() {
  drawSand(true);
  matrix.setCursor(1, 1);
  matrix.setTextColor(matrix.color565(0, 0, 0));
  matrix.print("The");
  matrix.setCursor(1,9);
  matrix.print("End");
}
